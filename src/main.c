#include <stdio.h>

#include "include/lwe_state.h"

int main() {
    LWE_Window* win = lwe_window_init(800, 600, "Hello WGPU");
    lwe_window_run(win);
}