#include "include/lwe_state.h"

#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>

LWE_Window* lwe_window_init(int width, int height, char* title) {
    if(glfwInit() != 1) {
        printf("Failed to initialize GLFW!\n");
        exit(-1);
    }
    LWE_Window* win = (LWE_Window*)malloc(sizeof(LWE_Window));
    LWE_State* state = lwe_state_init(win);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    win->win = glfwCreateWindow(width, height, title, NULL, NULL);
    return win;
}

void lwe_window_run(LWE_Window* win) {
    while(!glfwWindowShouldClose(win->win)) {
        lwe_state_update();
        lwe_state_render();
        glfwPollEvents();
    }
}