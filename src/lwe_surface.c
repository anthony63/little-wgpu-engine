#include "include/lwe_surface.h"
#include "include/wgpu/webgpu.h"
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

WGPUSurface lwe_init_surface(LWE_Window* win) {
    GLFWmonitor* monitor = glfwGetWindowMonitor(win->win);
    #ifdef _WIN32
    // TODO: Add windows support
    #elif __linux__
    Display* x11_dislay = glfwGetX11Display();
    Window x11_win = glfwGetX11Window(win->win);
    WGPUSurface surface = wgpuInstanceCreateSurface(
        NULL, 
        &(WGPUSurfaceDescriptor){
            .label = NULL,
            .nextInChain = (const WGPUChainedStruct*)&(
                WGPUSurfaceDescriptorFromXlibWindow
            ){
                .chain = (WGPUChainedStruct) {
                    .next = NULL,
                    .sType = WGPUSType_SurfaceDescriptorFromXlibWindow,
                },
                .display = x11_dislay,
                .window = x11_win,
            }
        });
    return surface;
    #endif
}