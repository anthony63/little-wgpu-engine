#pragma once

#include "wgpu/webgpu.h"

typedef struct {
    WGPUSwapChain chain;
    WGPUTextureFormat tex_fmt;
    WGPUTextureView tex_view;
    int width, height;
} LWE_Swap;
