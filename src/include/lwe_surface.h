#pragma once

#include "lwe_state.h"
#ifdef __WIN32
// TODO: Add Windows Support
#elif __linux__
#define GLFW_EXPOSE_NATIVE_X11
#include <GLFW/glfw3native.h>
#endif

#include "wgpu/webgpu.h"


WGPUSurface lwe_init_surface(LWE_Window* win);
