#pragma once

#include "wgpu/webgpu.h"
#include "lwe_swapchain.h"
#include <GLFW/glfw3.h>

typedef struct {
    WGPUSurface surface;
    WGPUDevice device;
    WGPUQueue queue;
    LWE_Swap swap;
} LWE_State;

typedef struct {
    GLFWwindow *win;
    LWE_State* state;
} LWE_Window;

LWE_Window* lwe_window_init(int width, int height, char* title);
void lwe_window_run(LWE_Window* win);

LWE_State* lwe_state_init(LWE_Window* win);
void lwe_state_render();
void lwe_state_update();

