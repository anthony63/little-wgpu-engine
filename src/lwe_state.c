#include "include/lwe_state.h"
#include "include/lwe_surface.h"
#include "include/wgpu/webgpu.h"
#include <stdio.h>
#include <stdlib.h>

void wgpu_on_request_adapter(WGPURequestAdapterStatus status, WGPUAdapter adapter, char* msg, void* userdata) {
    printf("[LWE] REQUESTING ADAPTER\n\tmsg: %s\n", msg);
    *(WGPUAdapter*)userdata = adapter;
}

LWE_State* lwe_state_init(LWE_Window* win) {
    LWE_State* state = (LWE_State*)malloc(sizeof(LWE_State));
    WGPUSurface surf = lwe_init_surface(win);
    
    WGPUAdapter adapter;
    wgpuInstanceRequestAdapter(NULL, &(WGPURequestAdapterOptions) {
        .nextInChain = NULL,
        .compatibleSurface = surf,
    }, (WGPURequestAdapterCallback)wgpu_on_request_adapter, &adapter);
    return state;
    
}

void lwe_state_render() {

}

void lwe_state_update() {

}
