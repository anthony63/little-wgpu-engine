CC = clang

SRC = src/*.c

OUT = bin/learning-wgpu

FLAGS = -lglfw -lGL -Llib -lwgpu_native -ldl -lX11 -lpthread -lm

$(OUT): $(SRC)
	$(CC) $(SRC) -o $(OUT) $(FLAGS)
run: $(OUT)
	./$(OUT)